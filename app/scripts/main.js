'use strict';

/* global Modernizr */

var transDuration = 300,
    handlerAssigned = false,
    pageChanging = false,
    tabFocus = false,
    $vcard = $('.vcard');

//"Double click" navigation for touch devices
// var touchedBefore;
// if(Modernizr.touch) {
//     $('[class^="profiles__"]').on('click', 'a', function(event) {
//         if(touchedBefore !== this) {
//             touchedBefore = this;
//             event.preventDefault();
//         }
//     });
// }

//Set aria-hidden and aria-selected attributes
$('.wrapper').attr('aria-hidden', 'true');
$('nav a').attr({'aria-selected': 'false', 'tabindex': '-1'});
$('#vcard').attr('aria-hidden', 'false');
$('#tab_vcard').attr({'aria-selected': 'true', 'tabindex': '0'});


//Change vCard by given page id
var changePage = function(id) {
    var $current = $('.current');
    if($current.length !== 1) { return; } //Might happen during transition
    var $next = $(id),
        $nextTab = $('[aria-controls=' + $next.attr('id') + ']');

    $('nav a').attr({'aria-selected': 'false', 'tabindex': '-1'});
    //$('[aria-controls=' + $current.attr('id') + ']').attr('aria-selected', 'false');
    $nextTab.attr({'aria-selected': 'true', 'tabindex': '0'});

    $current.attr('aria-hidden', 'true').removeClass('current');
    $next.attr('aria-hidden', 'false');

    //Let CSS transition handle the animation just by changing the class
    if(Modernizr.csstransitions) {
        $next.find('.vcard').css('opacity', 0);
        $next.addClass('current');
        setTimeout(function() {
            $next.find('.vcard').animate({opacity: 1}, transDuration);
        }, transDuration/2);
    } else {
        $next.addClass('current');
    }
    //Set correct focus
    if(tabFocus) {
        $nextTab.focus();
        tabFocus = false;
    } else {
        $next.focus();
    }
};

//Navigate pages (for scroll and swipe)
var navPage = function(link) {
    //prevent fast scrolling to change pages to fast (see debounce/throttle)
    if(pageChanging) { return; }
    setTimeout(function() {
        pageChanging = false;
    }, 1.5*transDuration + 100); //second transition is started with (transition/2) delay + extra threshold
    pageChanging = true && !tabFocus; //allow fast swichting for tabbing

    //Update hash
    var $link = $(link);
    if($link.length === 0) { return; }
    if(Modernizr.hashchange) {
        window.location.hash = $link.attr('href');
    } else {
        changePage($link.attr('href'));
    }
};
var nextPage = function() {
    navPage($('.current .nav__next'));
};
var previousPage = function() {
    navPage($('.current .nav__prev'));
};

//Let browser handle change of page by using hashtags
if(Modernizr.hashchange) {
    //Manual change of hashtag (bookmark etc.)
    $(window).on('hashchange', function(event) {
        var $page = $(location.hash);
        if($page.length > 0 && $page.hasClass('wrapper') && $page.attr('aria-hidden') === 'true') {
            event.preventDefault();
            changePage($page);
        }
    });

    //On page load trigger hash change
    $(window).trigger('hashchange');
} else {
    $vcard.on('click', '[class^="nav__"]', function(event) {
        event.preventDefault();
        changePage($(this).attr('href'));
    });
}

//Register events only for medium screen or wider
$(window).on('resize', function() {
    if($vcard.css('position') === 'absolute') {
        if(!handlerAssigned) {
            //Change vCard when scrolling or swiping
            $(document).on('mousewheel', function(event) {
                if (event.deltaY < 0) { //scrolling down
                    nextPage();
                } else { //scrolling up
                    previousPage();
                }
            });
            //Need to create swipe object, otherwise event handler below will not work
            $vcard.swipe({
                swipeDown: function() {},
                swipeUp: function() {}
            });
            $(document).on('swipeDown', '.vcard', function() {
                previousPage();
            });
            $(document).on('swipeUp', '.vcard', function() {
                nextPage();
            });
            handlerAssigned = true;
        }
    } else if (handlerAssigned) {
        $(document).off('mousewheel');
        $(document).off('swipeDown');
        $(document).off('swipeUp');
        $vcard.swipe('destroy');
        handlerAssigned = false;
    }
});

//On page load trigger resize
$(window).trigger('resize');

//Tab navigation
$('nav').on('focusin', function() {
    //$(this).siblings().css({'left': 'auto'});
    $('nav a').css({'left': 'auto'});
}).on('focusout', function() {
    $('nav a').css({'left': '-9999px'});
});
$('nav').on('keydown', 'a', function(event) {
    switch (event.which) {
        //left || up
        case 38:
        case 37:
            tabFocus = true;
            previousPage();
            break;

        //right || down
        case 40:
        case 39:
            tabFocus = true;
            nextPage();
            break;

        default:
            return;
    }
});

//Keyboard switching
$(document).on('keydown', function(event) {
    switch (event.which) {
        //up || pageUp
        case 38:
        case 33:
            previousPage();
            break;

        //down || pageDown
        case 40:
        case 34:
            nextPage();
            break;
    }
});

//Colorbox
$('a.colorbox').colorbox();

//Ajax email form
$('#contactform form').on('submit', function(event) {
    event.preventDefault();
    var $form = $(this),
        $submit = $form.find('[type=submit]');
    //lightboxMessage('Sending...');
    $submit.val('Sending...');
    $.post( $form.attr('action'), $form.serialize(), 'json'
     ).done(function(data) {
        if(data.error) {
            $submit.val(data.message);
        } else {
            $submit.val('Email successfully sent!');
            $form[0].reset();
            //$submit.val(data.message);
        }
        //lightboxMessage('Success');
        //$submit.val('Successfully sent!');
    }).fail(function() {
        //lightboxMessage('Error');
        $submit.val('Error while sending. Try again?');
    });
});