<?php

$is_ajax = false;
//Ajax Request?
if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    header('Content-type: application/json');
    $is_ajax = true;
}

if($_POST) {
    $emailto = 'mail@florian-stallmann.de';
    $subject = 'New email from vCard';

    //Sanitize input
    $form_name        = filter_var($_POST["name"], FILTER_SANITIZE_STRING);
    $form_email       = filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);
    $form_message     = filter_var($_POST["message"], FILTER_SANITIZE_STRING);
    $form_honeypot    = filter_var($_POST["surename"], FILTER_SANITIZE_STRING);

    //Validation
    if(strlen($form_honeypot) > 0) {
        if($is_ajax) {
            die(json_encode(array('error' => true, 'message' => 'Error: Captcha field not empty!')));
        } else {
            die('Sorry, honeypot captcha field was not empty. Please go back!');
        }
    }
    if(strlen($form_name) < 2) {
        if($is_ajax) {
            die(json_encode(array('error' => true, 'message' => 'Error: No name given!')));
        } else {
            die('Sorry, name too short. Please go back!');
        }
    }
    if(!filter_var($form_email, FILTER_VALIDATE_EMAIL)) {
        if($is_ajax) {
            die(json_encode(array('error' => true, 'message' => 'Error: Email not valid!')));
        } else {
            die('Sorry, email not valid. Please go back!');
        }
    }
    if(strlen($form_message) < 2) {
        if($is_ajax) {
            die(json_encode(array('error' => true, 'message' => 'Error: No message given!')));
        } else {
            die('Sorry, message too short. Please go back!');
        }
    }

    //Send email
    $header = 'From: ' . $form_email . "\r\n" .
        'Reply-To: ' . $form_email . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

    $sent = @mail($emailto, $subject, $form_name . " wrote:\r\n" . $form_message, $header);

    if(!$sent) {
        if($is_ajax) {
            die(json_encode(array('error' => true, 'message' => 'Unknown error while sending!')));
        } else {
            die('Sorry, email could not be sent!');
        }
    }
    if($is_ajax) {
        die(json_encode(array('error' => false, 'message' => 'Successfully sent!')));
    } else {
        die('Email successfully sent! Please go back now.');
    }
}

if(!headers_sent()) {
    header('Location: /');
}

/*if($is_ajax) {
    die(json_encode(array('error' => true, 'message' => 'Missing data!')));
} else {
    die('No post data!');
}*/