# Personal online vCard of Florian Stallmann

This is the source code of my online vCard on http://florian-stallmann.de. I used the yeoman webapp generator to build the page. You have to run `grunt install` and `bower install` to install all necessary components. Then you either run `grunt serve` for testing or `grunt build` for deploying the page.